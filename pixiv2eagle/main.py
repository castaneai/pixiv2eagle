import os
import tempfile
import itertools
from pixiv2eagle import eagle, pixiv


def mustenv(name: str) -> str:
    val = os.getenv(name)
    if val is None:
        raise LookupError(f"{name} is missing")
    return val


def run():
    eg = eagle.Eagle()

    session_id = mustenv("PIXIV_SESSION_ID")
    user_id = mustenv("PIXIV_USER_ID")
    f = pixiv.Fetcher(session_id)
    bookmarks = pixiv.get_all_bookmarks(f, user_id)
    for b in bookmarks:
        print(f"{b.title} {b.tags}")
        image_urls = f.get_original_image_urls(b)
        for url in image_urls:
            with tempfile.NamedTemporaryFile("wb", delete=False) as file:
                f.copy_image(url, file)
                eg.add_item(b.title, file.name, b.tags, b.url)


if __name__ == "__main__":
    run()