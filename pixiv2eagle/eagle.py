import requests
from typing import List


class Eagle:

    def __init__(self, base_url: str = "http://localhost:41595"):
        self._base_url = base_url

    def add_item(self, name: str, filename: str, tags: List[str], website: str):
        url = f"{self._base_url}/api/item/addFromPath"
        resp = requests.post(url, json={
            "path": filename,
            "name": name,
            "tags": tags,
            "website": website,
        }).json()
        if resp["status"] != "success":
            raise RuntimeError(f"eagle returned error: {resp}")
