import shutil
import datetime
import cloudscraper
import ratelimit
from dataclasses import dataclass
from typing import List, IO, Iterable


@dataclass(frozen=True)
class Artwork:
    id: str
    title: str
    created_at: datetime.datetime
    tags: List[str]
    page_count: int
    illust_type: int

    @classmethod
    def from_json(cls, data: dict):
        return Artwork(
            id=data["id"],
            title=data["title"],
            created_at=datetime.datetime.fromisoformat(data["createDate"]),
            tags=data["tags"],
            page_count=data["pageCount"],
            illust_type=data["illustType"],
        )

    @property
    def url(self):
        return f"https://www.pixiv.net/artworks/{self.id}"


class Fetcher:

    def __init__(self, session_id: str):
        self._scraper = cloudscraper.create_scraper()
        self._session_id = session_id

    @ratelimit.sleep_and_retry
    @ratelimit.limits(calls=1, period=5)
    def get_json(self, url: str):
        cookies = {"PHPSESSID": self._session_id}
        print(f"GET {url}")
        data = self._scraper.get(url, cookies=cookies).json()
        if data["error"]:
            raise RuntimeError(data["message"])
        return data["body"]

    def get_original_image_urls(self, artwork: Artwork):
        urls = []
        t = artwork.created_at
        for page in range(artwork.page_count):
            for ext in [".png", ".jpg", ".gif"]:
                url = f"https://i.pximg.net/img-original/img/{t.year}/{t.month:02}/{t.day:02}/{t.hour:02}/{t.minute:02}/{t.second:02}/{artwork.id}_p{page}{ext}"
                headers = {"referer": "https://www.pixiv.net/"}
                if self._scraper.head(url, headers=headers).status_code < 400:
                    urls.append(url)
                    break
        return urls

    @ratelimit.sleep_and_retry
    @ratelimit.limits(calls=1, period=5)
    def copy_image(self, image_url: str, dst: IO[bytes]):
        headers = {"referer": "https://www.pixiv.net/"}
        with self._scraper.get(image_url, stream=True, headers=headers) as resp:
            shutil.copyfileobj(resp.raw, dst)


def get_bookmarks(fetcher: Fetcher, user_id: str, offset: int, limit: int):
    url = f"https://www.pixiv.net/ajax/user/{user_id}/illusts/bookmarks?tag=&offset={offset}&limit={limit}&rest=show&lang=ja"
    return [Artwork.from_json(work) for work in fetcher.get_json(url)["works"]]


def get_all_bookmarks(fetcher: Fetcher, user_id: str) -> Iterable[Artwork]:
    offset = 0
    limit = 100
    while True:
        bookmarks = get_bookmarks(fetcher, user_id, offset, limit)
        if len(bookmarks) == 0:
            break
        for b in bookmarks:
            yield b
        offset += limit
